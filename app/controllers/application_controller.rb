class ApplicationController < ActionController::API
  #skip_before_action :verify_authenticity_token
  #attr_reader :current_user

  protected

  def authenticate_user #login
    user = User.find_for_database_authentication(email: params[:user][:email])
    if user.valid_password?(params[:user][:password])
      render json: payload(user)
    else
      render json: {errors: ['Invalid Username/Password']}, status: :unauthorized
    end
  end

  def authenticate_request #data request 
    unless user_id_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
    @current_user = User.find(auth_token[:user_id])
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  def authenticate_request #data request 
    unless user_id_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  def sso_cont
    render json: sso_payload(rand(0..100))
  end

  private

  def payload(user)
    return nil unless user and user.id
    {
      auth_token: JsonWebToken.encode({user_id: user.id}),
      user: {id: user.id, email: user.email}
    }
  end

  def sso_payload(id)

    {
      auth_token: JsonWebToken.encode({user_id: id}),
      userId: id,
      roles: ["role1", "role2", "role3"]
    }
  end

  def http_token
    puts "HTTP token: #{request.headers['Authorization'].split(' ').last}"
      @http_token ||= if request.headers['Authorization'].present?
        request.headers['Authorization'].split(' ').last
      end

  end

  def auth_token
    puts "auth_token #{JsonWebToken.decode(http_token)}"
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    puts "userrrr id #{auth_token[:user_id].to_i}"
    http_token && auth_token && auth_token[:user_id].to_i
  end
end
